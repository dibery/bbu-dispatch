#ifndef BBU_H
#define BBU_H

#include"common.h"

class BBU
{
	public:
		BBU();
		BBU( double, double, time_t, time_t );
		BBU( const BBU& );
		virtual ~BBU();
		time_t runtime();
		void operator= ( const BBU& rhs );

//member functions
        TP T, TQ; // The time being served / time created
		vector< pair< Interval, Server& > > history; // history = a, class interval = tau

//data members
		double cpu_req, ram_req;
		time_t timeout, q_timeout;
		int my_id;

		static int id;
};

#endif // BBU_H
