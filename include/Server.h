#ifndef SERVER_H
#define SERVER_H

#include"common.h"

class Server
{
private:
	typedef list<BBU> Task;
	typedef Task::iterator Task_it;
    bool on;

public:
    Server();
    Server( unsigned, unsigned, double, double, double, double, double, double, double );
    virtual ~Server();

//member functions
    bool ok( const BBU& );
    bool empty() { return task.empty(); }
    bool power() const { return on; }
    bool low() { return cpu_utility() < cpu_off || ram_utility() < ram_off; }
    bool high() { return cpu_utility() > cpu_off || ram_utility() > ram_off; }
    void power( bool p );
    void take( BBU, bool );
    void drop();
    void erase( Task_it i ) { task.erase( i ); }
    void move( Task_it, Server& );
    void moniter();
	double cpu_utility();
    double ram_utility();
    double cost(); // Find fix & variable costs here
    time_t time_on();
    time_t time_off();
    Task_it begin() { return task.begin(); }
    Task_it end() { return task.end(); }
    void operator= ( const Server& );

//data members
    unsigned cpu_core, ram_capacity; // GB
    double cpu_freq, computing_reserve_ratio; // GHz, % for os
    double cpu_on, cpu_off, ram_on, ram_off;
    double cpu_left, ram_left;
    double fix, var; // fixed / variable cost
    int my_id, on_count;
    TP T;
    Task task;
    vector<pair<Interval,double>> history;

    static int id, last_open;
    static const bool SW_ON = true, SW_OFF = false;
    static mutex s_mtx;
};

#endif // SERVER_H
