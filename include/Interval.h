#ifndef INTERVAL_H
#define INTERVAL_H

#include"common.h"

class Interval
{
	public:
//member functions
		Interval( TP, TP );
		Interval( const Interval& );
		virtual ~Interval();
		void operator= ( const Interval& );

		TP begin, end;
		DUR span;
		long long length() { return span.count(); }
//data members

};

#endif // INTERVAL_H
