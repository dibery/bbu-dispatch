#include "common.h"

const int SERVER_NUM = 10, TASK_NUM = 100, NPOS = -1;
const double CAP_RATE = .25;
// System initialization
int level[ SERVER_NUM ];
bool finish = false;
Server* pool = nullptr;//new Server[ SERVER_NUM ];
Queue<BBU> waiting;
thread *world = nullptr;//new thread[ SERVER_NUM ];
mutex mtx;
FILE *f, *f2;

void exec( const int sid )
{
//	pool[ sid ] = Server( 8 * ( 1 + level[ sid ] * CAP_RATE ), 64 * ( 1 + level[ sid ] * CAP_RATE ), 3, .02, .9, .1, .9, .1, 1 * ( 1 + level[ sid ] * .25 ) );

	mtx.lock();
//	if ( sid == 0 )
//		pool[ sid ].power( Server::SW_ON ); // Turn on the first one
	mtx.unlock();

	while( !finish || !pool[ sid ].empty() )
	{
		mtx.lock();
		if ( pool[ sid ].empty() || !pool[ sid ].power() )
		{
			mtx.unlock();
			continue;
		}
		mtx.unlock();
		// Turn off server, Migration BBU,
		mtx.lock();
		if ( !finish && pool[ sid ].power() /*&& sid*/ && pool[ sid ].low() && !pool[ sid ].empty() )
		{
			for ( int k = 0; k < SERVER_NUM; ++k )
				if ( k != sid && pool[ k ].power() && pool[ k ].ok( pool[ sid ].task.front() ) )
				{
					pool[ sid ].move( pool[ sid ].begin(), pool[ k ] );
					break;
				}
			pool[ sid ].drop();
			if ( pool[ sid ].empty() /*&& sid*/ )
				pool[ sid ].power( Server::SW_OFF );
			mtx.unlock();
			continue;
		}
//		mtx.unlock();

		// Turn on another server
//		mtx.lock();
//		if ( !finish && pool[ sid ].high() && Server::last_open != sid &&
//			all_of( pool, pool + SERVER_NUM, [] ( Server& s ) { return s.power()? !s.low() : true; } ) )
//			for ( int j = 0; j < SERVER_NUM; ++j )
//				if ( !pool[ j ].power() )
//				{
//					pool[ j ].power( Server::SW_ON );
//					j = SERVER_NUM;
//					Server::last_open = sid;
//				}

		//Remove completed tasks of me
		pool[ sid ].drop();
		if ( pool[ sid ].empty() /*&& sid*/ )
			pool[ sid ].power( Server::SW_OFF );
		mtx.unlock();
	}
	// All tasks are done now
	mtx.lock();
	cout << "Closing server " << sid + 1 << endl << endl;
	pool[ sid ].moniter();
	pool[ sid ].power( Server::SW_OFF );
	cout << "Server " << sid + 1 << " joined" << endl << endl;
	assert( pool[ sid ].empty() );
	assert( !pool[ sid ].power() );
	mtx.unlock();
}

void wait()
{
	//task initialization
	for ( int i = 0; i < TASK_NUM; ++i )
		waiting.push( BBU( rand() % 8 + 1, rand() % 32, rand() % 2000 + 1000, rand() % 2000 + 1000 ) ); // CPU, RAM, Timeout (ms)
//		waiting.push( BBU( rand() % 8 + 1, rand() % 32, rand() % 2000 + 1000, INT_MAX ) ); // CPU, RAM, Timeout (ms)
	mtx.lock();
	mtx.unlock();

	while( !waiting.empty() )
	{
		BBU& f = waiting.front();
		int assign = NPOS;

		// Expired before it can enter a server
		if ( Interval( f.TQ, high_resolution_clock::now() ).length() > f.q_timeout )
		{
			waiting.pop();
			++waiting.blocked_bbu;
			printf( "BBU %d expired in queue...\n\n", f.my_id );
			continue;
		}

		// Take BBU front queue assigned into server i, pop queue
		mtx.lock();
		for ( int i = 0; i < SERVER_NUM && assign == NPOS; ++i )
			if( pool[ i ].power() && pool[ i ].ok( f ) )
				assign = i;
		if ( assign != NPOS )
		{
			cout << "Assigning BBU " << f.my_id << " to server " << assign + 1 << endl << endl;
			pool[ assign ].take( f, false );
			waiting.pop();
		}
		else if ( !all_of( pool, pool + SERVER_NUM, [] ( const Server& s ) { return s.power(); } ) )
		{
			cout << "No one OK. ";
			for ( int i = 0; i < SERVER_NUM; ++i )
				if ( !pool[ i ].power() && pool[ i ].ok( f ) )
				{
					cout << "Assigning BBU " << f.my_id << " to server " << i + 1 << endl << endl;
					pool[ i ].power( Server::SW_ON );
					pool[ i ].take( f, false );
					waiting.pop();
					break;
				}
				else if ( i == 0 && waiting.size() == TASK_NUM )
				{
					cout << pool[ i ].cpu_left << '\t' << pool[ i ].ram_left << '\t' << f.cpu_req << '\t' << f.ram_req << endl;
				}
		}
		mtx.unlock();
	}
	finish = true;
}

void simulation()
{
	srand( 0 );
	//TP start_time = high_resolution_clock::now();
	finish = false;
	pool = new Server[ SERVER_NUM ];
	world = new thread[ SERVER_NUM ];
	waiting.blocked_bbu = 0;

	//server initialization
	mtx.lock();
	for ( int i = 0; i < SERVER_NUM; ++i )
		pool[ i ] = Server( 8 * ( 1 + level[ i ] * CAP_RATE ), 64 * ( 1 + level[ i ] * CAP_RATE ), 3, .02, .9, .1, .9, .1, 1 * ( 1 + level[ i ] * .25 ) );
	for ( int i = 0; i < SERVER_NUM; ++i )
		world[ i ] = thread( exec, i );
	thread customer( wait );
	mtx.unlock();

	customer.join();
	for ( int i = 0; i < SERVER_NUM; ++i )
		world[ i ].join();
	puts( "All tasks are done now. Calculate cost..." );
	int total = 0;
	for ( int i = 0; i < SERVER_NUM; ++i )
		total += pool[ i ].cost();
	for ( int i = 0; i < SERVER_NUM; ++i )
		cout << "Cost of server " << i + 1 << " = " << pool[ i ].cost() << "\tfix cost = " << pool[ i ].fix * pool[ i ].on_count << endl;
//	fprintf( stdout, "%d\n", total ),
	fprintf( f, "%d\n", total );	// total cost
	printf( "%d BBU(s) expired in queue\n", waiting.blocked_bbu );
	fprintf( f2, "%d\n", waiting.blocked_bbu );
	cout << endl;

		/*
	FILE *fout = fopen( "C:\\model3\\model3\\bin\\Debug\\plane.txt", "w" );
	for ( int i = 0; i < SERVER_NUM; ++i )
	{
		if ( pool[ i ].history.empty() )
			continue;
		long long qnt = Interval( start_time, pool[ i ].history.back().first.end ).length(), *data = new long long[ qnt ];

		memset( data, 0, sizeof( long long ) * qnt );
		for ( auto j: pool[ i ].history )
		{
			int s = Interval( start_time, j.first.begin ).length(), e = Interval( start_time, j.first.end ).length();
			for ( int k = s; k < e; ++k )
				data[ k ] = j.second * 100;
		}
		for ( int k = 0; k < qnt; ++k )
			fprintf( fout, "%d\t%I64d\n", k, data[ k ] );
		fprintf( fout, "\n" );
		delete[] data;
	}
	*/

	delete[] pool;
	delete[] world;
}

void type( int types, int depth = 0, int filled = 0 )
{
	if ( depth < types )
		for ( int i = 0; i + filled <= SERVER_NUM; ++i )
		{
			for ( int j = filled; j < filled + i; ++j )
			{
				level[ j ] = depth;
			}

			type( types, depth + 1, filled + i );
		}
	else if ( filled == SERVER_NUM )
		simulation();
//		for ( int i = 0; i < SERVER_NUM; ++i )
//			printf( "%d%c", level[ i ], i == SERVER_NUM - 1? '\n' : '\t' );
}

int main( int argc, char* argv[] )
{
	if ( argc == 2 )
		f = fopen( argv[ 1 ], "w" ), f2 = fopen( "drop.txt", "w" );
	else if ( argc == 1 )
		f = fopen( "cost.txt", "w" ), f2 = fopen( "drop.txt", "w" );
	else if ( argc == 3 )
		f2 = fopen( argv[ 2 ], "w" ), f = fopen( argv[ 1 ], "w" );
	type( 3 );
}
