#include "Server.h"

int Server::id = 0, Server::last_open = -1;
mutex Server::s_mtx;

Server::Server( unsigned cc, unsigned rc, double cf, double crr, double con, double coff, double ron, double roff, double v )
{
    cpu_left = ( cpu_core = cc ) * cf, ram_left = ram_capacity = rc;
    cpu_freq = cf, computing_reserve_ratio = crr;
    cpu_on = con, cpu_off = coff, ram_on = ron, ram_off = roff;
    on = false;
    my_id = ++id;
    fix = 3.3 * cc * cf + 2.25 * rc;
    var = v;
    on_count = 0;
}

Server::Server()
{
	my_id = ++id;
	on = false;
}

Server::~Server()
{
    //dtor
}

void Server::operator= ( const Server& rhs )
{
	cpu_core = rhs.cpu_core, ram_capacity = rhs.ram_capacity,
	cpu_freq = rhs.cpu_freq, computing_reserve_ratio = rhs.computing_reserve_ratio,
	cpu_on = rhs.cpu_on, cpu_off = rhs.cpu_off, ram_on = rhs.ram_on, ram_off = rhs.ram_off,
	cpu_left = rhs.cpu_left, ram_left = rhs.ram_left,
	on = rhs.on,
//	my_id = rhs.my_id,
	T = rhs.T,
	task = rhs.task,
	history = rhs.history;
	fix = rhs.fix;
	var = rhs.var;
	on_count = rhs.on_count;
}
bool Server::ok( const BBU& bbu )
{
	return cpu_left >= bbu.cpu_req && ram_left >= bbu.ram_req;
}

void Server::power( bool p )
{
	if ( on == p )
	{
		puts( "Same power status\n" );
		return;
	}
	s_mtx.lock();
//	on = p;
	if( ( on = p ) )
	{
		T = high_resolution_clock::now();
		++on_count;
		auto tt = system_clock::to_time_t( T );
		cout << "Server " << my_id << " turned on at: " << ctime( &tt ) << endl;
		moniter();
	}
	else
	{
		TP now = high_resolution_clock::now();
		auto tt = system_clock::to_time_t( now );
		cout << "Server " << my_id << " turned off at: " << ctime( &tt ) << endl;
		moniter();
	}
	s_mtx.unlock();
}

void Server::take( BBU bbu, bool lock = true )
{
	if ( on == SW_OFF )
		return;
	if ( lock )
		s_mtx.lock();
	moniter();
	cpu_left -= bbu.cpu_req;
	ram_left -= bbu.ram_req;
	bbu.T = high_resolution_clock::now();
	auto tt = system_clock::to_time_t( high_resolution_clock::now() );
	cout << "Server " << my_id << " took BBU " << bbu.my_id << " at: " << ctime( &tt ) << endl;
	task.push_back( bbu );
	moniter();
	if ( lock )
		s_mtx.unlock();
}

void Server::drop()
{
	if ( on == SW_OFF )
		return;
	s_mtx.lock();
	moniter();
	for ( auto i = task.begin(); i != task.end(); )
		if ( duration_cast<DUR>( high_resolution_clock::now() - i->T ).count() >= i->timeout )
		{
			cpu_left += i->cpu_req;
			ram_left += i->ram_req;
			auto tt = system_clock::to_time_t( high_resolution_clock::now() );
			cout << "Task " << i->my_id << " is dropped at " << ctime( &tt ) << endl;
			i = task.erase( i );
		}
		else
			++i;
	moniter();
	s_mtx.unlock();
}

void Server::move( Task_it t, Server& s )
{
	if ( s.ok( *t ) )
	{
		s_mtx.lock();
		moniter();
		cout << "Move BBU " << t->my_id << " from server " << my_id << " to server " << s.my_id << endl << endl;
		s.take( *t, false );
		cpu_left += t->cpu_req;
		ram_left += t->ram_req;
		task.erase( t );
		s_mtx.unlock();
	}
}

double Server::cpu_utility()
{
	return ( cpu_core * cpu_freq - cpu_left ) / ( cpu_core * cpu_freq );
}

double Server::ram_utility()
{
	return ( ram_capacity - ram_left ) / ram_capacity;
}

double Server::cost()
{
	long long ret = 0;

//	cout << "Server " << my_id + 1 << " history:" << endl;
//	cout << "Fixed cost = " << fix << " Turned on " << on_count << " times" << endl;
//	for ( auto& i: history ) // i is pair<interval,double>
//		if ( i.first.length() )
//		cout << var << '\t' << i.first.length() << '\t' << i.second << endl;
//	cout << endl;
	for ( auto& i: history ) // i is pair<interval,double>
		ret += var * i.first.length() * i.second;

	return ret + fix * on_count;
}

void Server::moniter()
{
	if ( history.empty() )
		history.push_back( pair<Interval,double>( Interval( T, high_resolution_clock::now() ), cpu_utility() ) );
	else
		history.push_back( pair<Interval,double>( Interval( history.back().first.end, high_resolution_clock::now() ), cpu_utility() ) );
}
