#include "BBU.h"

int BBU::id = 0;

BBU::BBU( double cpu = 1, double ram = 1, time_t expired = 900, time_t q_expired = 900 ) : cpu_req( cpu ), ram_req( ram ), timeout( expired ), q_timeout( q_expired )
{
	my_id = ++id;
	TQ = high_resolution_clock::now();
}

BBU::BBU( const BBU& rhs )
{
	cpu_req = rhs.cpu_req;
	ram_req = rhs.ram_req;
	timeout = rhs.timeout;
	q_timeout = rhs.q_timeout;
	my_id = rhs.my_id;
	T = rhs.T;
	TQ = rhs.TQ;
}

BBU::BBU()
{
}

BBU::~BBU()
{
	//dtor
}

void BBU::operator= ( const BBU& rhs )
{
	cpu_req = rhs.cpu_req;
	ram_req = rhs.ram_req;
	timeout = rhs.timeout;
	q_timeout = rhs.q_timeout;
	my_id = rhs.my_id;
	T = rhs.T;
	TQ = rhs.TQ;
}
